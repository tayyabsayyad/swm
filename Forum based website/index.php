<?php include('header.php'); ?>
<?php
session_start();
?>
<div id="content">
	<div id="colOne">
		<div id="latest-post">
			<h2 class="title"><a href="#">Welcome to SWM!</a></h2>
			<h3 class="posted"></h3>
			<div class="story">
				<p>This is forum based website where students, teachers can suggest and put their ideas over here to reduce the waste. The ideas can be discussed by anyone.
			</div>
		</div>
		
		<div id="recent-posts">
			<h3 class="title">Recent Posts</h3>
			<ul>
				<?php
				$threads = $db->get('subject', 5); 
				if($db->count >0)
				{
					foreach ($threads as $thread ) {
						# code...
					
				?>
				<li><strong><a href="view_thread.php?id=<?= $thread['id']?>"><?= $thread['subject'] ?></a></strong><br />
					<small>Filed under <a href="#" class="category">

					<?php
					$cat = $db->where('id',$thread['grp_id'])->getOne('groups');
					
					echo $cat['name'];
					?>	

					</a> | <a href="#" class="comment">
					<?php
					$comment = $db->where('sub_id',$thread['id'])->get('comments');
					echo $db->count;
					?>
					 Comment &raquo;</a></small></li>
				<?php } }
				else {
					echo "<li><strong>No Threads </strong></li>";
				}?>
				<!-- <li><strong><a href="#">Thread 2</a></strong><br />
					<small>Filed under <a href="#" class="category">Uncategorized</a> | <a href="#" class="comment">1 Comment &raquo;</a></small></li>
				<li><strong><a href="#">Thread 3</a></strong><br />
					<small>Filed under <a href="#" class="category">Uncategorized</a> | <a href="#" class="comment">1 Comment &raquo;</a></small></li> -->
				<!-- <li><strong><a href="#">Quisque in dolor non erat</a></strong><br />
					<small>Filed under <a href="#" class="category">Uncategorized</a> | <a href="#" class="comment">1 Comment &raquo;</a></small></li>
				<li><strong><a href="#">Fusce lobortis dui molestie eros</a></strong><br />
					<small>Filed under <a href="#" class="category">Uncategorized</a> | <a href="#" class="comment">1 Comment &raquo;</a></small></li>
				<li><strong><a href="#">Maecenas vel leo nec dui pulvinar</a></strong><br />
					<small>Filed under <a href="#" class="category">Uncategorized</a> | <a href="#" class="comment">1 Comment &raquo;</a></small></li> -->
			</ul>
		</div>
		<div style="clear: both; height: 1px;"></div>
	</div>
	<?php include('sidebar.php');?>
</div>
<?php include('footer.php');?>


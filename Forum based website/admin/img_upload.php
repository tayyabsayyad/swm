<?php
session_start();
if(!$_SESSION['login'] == 'admin'){
    header("location:page-login.php");
}
?>

<?php
require_once "../db/MysqliDb.php";
error_reporting(E_ALL);
$db = new MysqliDb();
//$pages = $db->get('pages');

$uploadOk = 0; //for notification message
$db = new MysqliDb();
//$pages = $db->get('pages');
$id = $_GET['id'];

?>
<?php

if($_SERVER['REQUEST_METHOD'] == "POST"){

        $target_dir = "myimage/";
        $target_file = str_replace(" ","_",$target_dir . basename($_FILES["fileToUpload"]["name"]));
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
         $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
         if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
        $ins_content = $db->where ('id', $id)->update ('image_path', Array("img_path"=>$target_file));
        if($ins_content)
        {
        $uploadOk = 1;
    }
}
    } else {
        
        $uploadOk = 0;
    }
}
        
        


?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->
    <?php include 'language/leftpanel.php'  ?>
     <!-- Left Panel -->   

    <!-- Right Panel -->
    <?php include 'language/rightpanel.php'  ?>
    <!-- Right Panel -->

        <?php
        $db->where ('cat_id', $id);
        $pages = $db->get ('pages_desc');
        ?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                    </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Pages</a></li>
                            <li class="active"><?php echo $pages[0]['tab'] ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                  <div>

                    <div class="card">
                      <div class="card-header">
                        <strong><?php echo $pages[0]['name'] ?></strong> tab
                      </div>
                      <div class="card-body">
                                        <?php if($uploadOk)
                                        {?>
                                        <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show" name="notify">
                                            <span class="badge badge-pill badge-primary">Success</span>
                                                Successfully Updated.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <?php } ?>
                      <div class="card-body card-block">
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?id=<?php echo $pages[0]['cat_id']?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Select image to upload:</label>
                        
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        </div>
                        </div>
                        
                          
                        
                      <div class="card-footer">
                        <button type="submit" name="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Upload Image
                        </button>
                      <!--   <button type="reset" name="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button> -->
                      </div>
                      </form>
                      </div>
                    </div>
                  </div>

                  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>

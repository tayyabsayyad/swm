<?php
session_start();
if(!$_SESSION['login'] == 'admin'){
    header("location:page-login.php");
}
?>

<?php
require_once "../db/MysqliDb.php";
error_reporting(E_ALL);
$db = new MysqliDb();
//$pages = $db->get('pages');

$act = 0; //for notification message
$db = new MysqliDb();
//$pages = $db->get('pages');
$id = $_GET['id'];

?>
<?php
if($_SERVER['REQUEST_METHOD'] == "POST"){


        $title = $_POST['title'];
        $content = $_POST['content'];
        $ins_content = $db->where ('cat_id', $id)->update ('pages_desc', Array("name"=>$title,"description"=>$content));
        //$password = $_POST['pass'];
        
        // $query = "SELECT * FROM users WHERE uname = '$username' AND password = '$password'";
        if($ins_content)
        {
             $act = 1;
        }
        else
        echo 'update failed: ' . $db->getLastError();
}

?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->
    <?php include 'language/leftpanel.php'  ?>
     <!-- Left Panel -->   

    <!-- Right Panel -->
    <?php include 'language/rightpanel.php'  ?>
    <!-- Right Panel -->

        <?php
        $db->where ('cat_id', $id);
        $pages = $db->get ('pages_desc');
        ?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Pages</a></li>
                            <li class="active"><?php echo $pages[0]['tab'] ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                  <div>

                    <div class="card">
                      <div class="card-header">
                        <strong><?php echo $pages[0]['name'] ?></strong> tab
                      </div>
                      <div class="card-body">
                                        <?php if($act==1)
                                        {?>
                                        <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show" name="notify">
                                            <span class="badge badge-pill badge-primary">Success</span>
                                                Successfully Updated.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <?php } ?>
                      <div class="card-body card-block">
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?id=<?php echo $pages[0]['cat_id']?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                      
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Title</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="title" value="<?php echo $pages[0]['name'] ?>" class="form-control"></div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="textarea-input" class=" form-control-label">Content</label></div>
                            <div class="col-12 col-md-9"><textarea name="content" id="textarea-input" rows="9"  class="form-control"><?php echo $pages[0]['description'] ?></textarea></div>
                          </div>
                          
                        
                      <div class="card-footer">
                        <button type="submit" name="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                      <!--   <button type="reset" name="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button> -->
                      </div>
                      </form>
                      </div>
                    </div>
                  </div>

                  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>

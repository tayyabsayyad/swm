<?php
session_start();
if(!$_SESSION['login'] == 'admin'){
    header("location:page-login.php");
}
?>

<?php
require_once "../db/MysqliDb.php";
date_default_timezone_set("Asia/Kolkata");
// require_once "../language/mail_launguage.php";
error_reporting(E_ALL);
$db = new MysqliDb();
//$pages = $db->get('pages');
$mdg= "";
$act = 0; //for notification message
// $db = new MysqliDb();
//$pages = $db->get('pages');


?>
<?php
if($_SERVER['REQUEST_METHOD'] == "POST"){


        $ques = $_POST['ques'];
        $grp_id = $_POST['grp_id'];
        // $pass1 = $_POST['pass1'];
        // $pass2 = $_POST['pass2'];
        

        if(!empty($ques))
        {
            if((int)$grp_id>0)
            {
            $id = $db->insert ('subject', Array("user_id"=>$_SESSION['admin_id'],"grp_id"=>$grp_id,"subject"=>$ques,"date"=>date('Y-m-d H:i:s')));
            if($id)
            {
                $msg = "Question Successfully Created";
                $act = 1;
            }
            }
            else
            {
                $act = 2;
                $msg = "Please select a group.";
            }   
        }
        else
        {
            $act = 2;
            $msg = "Please fill all the fields.";            
        }
        //$password = $_POST['pass'];
        
        // $query = "SELECT * FROM users WHERE uname = '$username' AND password = '$password'";
        // if($ins_content)
        // {
        //      $act = 1;
        // }
        // else
        // echo 'update failed: ' . $db->getLastError();
}

?>


<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="assets/scss/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body>
        <!-- Left Panel -->
    <?php include 'language/leftpanel.php'  ?>
     <!-- Left Panel -->   

    <!-- Right Panel -->
    <?php include 'language/rightpanel.php'  ?>
    <!-- Right Panel -->

        
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Dashboard</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Pages</a></li>
                            <li class="active">Create Thread</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">

                  <div>

                    <div class="card">
                      <div class="card-header">
                        <strong>Create Thread</strong> tab
                      </div>
                      <div class="card-body">
                                        <?php if($act==1)
                                        {?>
                                        <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show" name="notify">
                                            <span class="badge badge-pill badge-primary">Success</span>
                                                Successfully Created.
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <?php } elseif($act==2) { ?>
                                        <div class="alert alert-danger" role="alert">
                                            <?php echo $msg ?>
                                        </div>
                                        <?php } ?>
                      <div class="card-body card-block">
                        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                      
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Question</label></div>
                            <div class="col-12 col-md-9"><textarea rows="4" cols="50" name="ques"></textarea>
                          </div>

                          <!-- <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">New Password</label></div>
                            <div class="col-12 col-md-9"><input type="password" id="text-input" name="pass1" class="form-control"></div>
                          </div><div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">Retype Password</label></div>
                            <div class="col-12 col-md-9"><input type="password" id="text-input" name="pass2" class="form-control"></div> -->
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3"><label for="select" class=" form-control-label">Assign To:</label></div>
                            <div class="col-12 col-md-9">
                              <select name="grp_id" id="select" class="form-control">
                                <option value="0">Please select</option>
                                
                                <?php
                                $grp_name = $db->get('groups');
                                foreach ($grp_name as $grp) {
                                  echo "<option value=".$grp['id'].">".$grp['name']."</option>";
                                    # code...
                                }
                                ?>
                                
                              </select>
                            </div>
                          </div>
                        
                      <div class="card-footer">
                        <button type="submit" name="submit" class="btn btn-primary btn-sm">
                          <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                      <!--   <button type="reset" name="reset" class="btn btn-danger btn-sm">
                          <i class="fa fa-ban"></i> Reset
                        </button> -->
                      </div>
                      </form>
                      </div>
                    </div>
                  </div>

                  
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="assets/js/vendor/jquery-2.1.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>
